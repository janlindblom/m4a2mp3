# M4a2mp3

A simple m4a to mp3 transcoding utility. The main purpose of this tool is to convert audio books in m4a or m4b format (AAC) to mp3 to make them work on my mother's mp3 player (which is a rather simple one).

**Note:** Make sure you own the source files, don't live in the UK[^ukcopying] and that you are legally allowed to make copies of the source files! **The author of this tool takes no responsibility of how you may use it.**

## Prerequisites

Make sure you have `ffmpeg` installed with support for AAC decoding and MP3 encoding, on a Mac this can be installed using Homebrew:

    $ brew install ffmpeg --with-faac

## Installation

Install it as such:

    $ gem install m4a2mp3

## Usage

Run it thusly:

    $ m4a2mp3 transcode

In a directory where you have a bunch of m4a, m4b or m4r files you want to convert to mp3.

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release` to create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

1. Fork it ( https://bitbucket.org/janlindblom/m4a2mp3/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

[^ukcopying]: See [The Copyright and Rights in Performances (Personal Copies for Private Use) Regulations 2014](http://www.legislation.gov.uk/uksi/2014/2361/made)