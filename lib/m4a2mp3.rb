require "m4a2mp3/version"
require "m4a2mp3/processors"
require "thor"
require "ruby-progressbar"
require "parallel"

# A simple m4a to mp3 transcoding utility.
# 
# @author Jan Lindblom <janlindblom@fastmail.fm>
# @license MIT
# @version 1.0.0
module M4a2mp3
  # The main application.
  # 
  # @since 1.0.0
  class CLI < Thor
    package_name "m4a2mp3"

    desc "transcode", "Convert m4a, m4b or m4r files to mp3 using ffmpeg."
    # The main task, complete with error handling.
    def transcode
      hasffmpeg = `which ffmpeg`

      unless hasffmpeg.empty?
        @ffmpeg = hasffmpeg.strip
        ffmpeg_has_aac = (`#{@ffmpeg} -loglevel quiet -formats | grep aac`).match(/DE?/)
        ffmpeg_has_mp3 = (`#{@ffmpeg} -loglevel quiet -formats | grep mp3`).match(/D?E/)
        if ffmpeg_has_aac
          if ffmpeg_has_mp3
            files = Dir['*.m4*']
            unless files.empty?
              say "Transcoding #{files.count} files on #{Processors.count} cores..."
              Parallel.each(files, progress: "Converting files...", in_processes: Processors.count) do |file|
                fparts = file.split '.'
                res = system "#{@ffmpeg} -y -loglevel quiet -i #{file} #{fparts[0]}.mp3"
                res
              end
              return 0
            else
              say "No input files, exiting."
            end
          else
            say "ffmpeg cannot encode mp3, please make sure you have installed ffmpeg with mp3 encoder support."
          end
        else
          say "ffmpeg cannot decode aac, please make sure you have installed ffmpeg with aac decoder support."
        end
      else
        say "ffmpeg is not installed, please install ffmpeg and make sure it is available in your $PATH."
      end
      return 255
    end
  end
end
