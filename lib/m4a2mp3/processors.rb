require 'parallel/processor_count'

module M4a2mp3
  # Wrapper around Parallel::ProcessorCount because I want to print out the
  # number of jobs that will run in parallel.
  # 
  # @since 1.0.0
  # @api private
  class Processors
    extend Parallel::ProcessorCount

    # Display the number of processor cores visible to the operating system.
    # 
    # @return [Integer] the number of processor cores
    # @api private
    def self.count
      processor_count
    end
  end
end